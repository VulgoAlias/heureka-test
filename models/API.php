<?php

namespace app\models;

use yii\httpclient\Client;

class API extends \yii\base\BaseObject
{
	private $_client;
	private $_productCategory;
	
	public function __construct()
	{
		$this->_productCategory = [];
		$this->_client = new Client(
			[
				'baseUrl' => 'https://heureka-testday.herokuapp.com/'
			]
		);
	}
	
	public function getCategoryList()
	{
		$request = $this->_client->createRequest();
		$request->setUrl('categories');
		$request->setFormat(Client::FORMAT_JSON);
		$response = $request->send();
		
		return $response->getData();
	}
	
	public function getCategoryDetail(int $Id)
	{
		$request = $this->_client->createRequest();
		$request
			->setUrl('category/'.$Id)
			->setFormat(Client::FORMAT_JSON)
		;
		
		$response = $request->send();
		
		return $response->getData();
	}	
	
	public function getCategoryProducts(int $catId, int $offset = NULL, int $limit = NULL)
	{
		if(is_null($limit) && is_null($offset))
		{
			$offset = "";
			$limit = "";
		}
		elseif(is_null($offset))
		{
			$offset = "/";
			$limit = "/".$limit;
		}
		elseif(is_null($limit))
		{
			$limit = "";
		}
		$url = 'products/'.$catId."/".$offset.$limit;
		$request = $this->_createClientRequest($url);
		$response = $request->send();
		$products = $response->getData();
		
		foreach($products as $pKey => $product)
		{
			if(!array_key_exists($pKey, $this->_productCategory))
			{
				$this->_productCategory[$pKey] = $catId;
			}
		}
		
		return $products;		
	}
	
	public function getProductDetail(int $prodId)
	{
		$url = 'product/' . $prodId;
		$request = $this->_createClientRequest($url);
		$response = $request->send();
		$product = $response->getData();
		
		return $product;
	}
	
	public function getProductOffers(int $prodId, int $offset = NULL, $limit = NULL)
	{
		if(is_null($limit) && is_null($offset))
		{
			$offset = "";
			$limit = "";
		}
		elseif(is_null($offset))
		{
			$offset = "/";
			$limit = "/".$limit;
		}
		elseif(is_null($limit))
		{
			$limit = "";
		}
		
		$url = 'offers/' . $prodId . '/' . $offset . $limit;
		$request = $this->_createClientRequest($url);
		$response = $request->send();
		
		return $response->getData();
	}
	
	public function completeProductFromOffers(array &$product)
	{
		$offers = $this->getProductOffers($product['productId']);
		$imageFull = FALSE;
		$descriptionFull = FALSE;

		foreach($offers as $offer)
		{
			if($offer['img_url'] && $offer['description'])
			{
				$product['img_url'] = $offer['img_url'];
				$product['description'] = $offer['description'];
				
				$imageFull = TRUE;
				$descriptionFull = TRUE;
				break;
			}
			elseif($imageFull === FALSE && $offer['img_url'])
			{
				$product['img_url'] = $offer['img_url'];
				$imageFull = TRUE;
				
				if($descriptionFull === TRUE)
				{
					break;
				}
			}
			elseif($descriptionFull === FALSE && $offer['description'])
			{
				$product['description'] = $offer['description'];
				$descriptionFull = TRUE;
				
				if($imageFull === TRUE)
				{
					break;
				}
			}
		}
		
		if($imageFull === FALSE)
		{
			$product['img_url'] = "https://via.placeholder.com/150";
		}
		
		if($descriptionFull === FALSE)
		{
			$product['description'] = "";
		}
		
		return ['img' => $imageFull, 'desc' => $descriptionFull];
	}
	
	public function getCategoryPreviewImage(int $categoryId)
	{
		$products = $this->getCategoryProducts($categoryId);
		$response = "";
		
		foreach($products as $product)
		{
			$offers = $this->getProductOffers($product['productId']);

			foreach ($offers as $offer)
			{
				if($offer['img_url'])
				{
					$response = $offer['img_url'];
					break;
				}
			}
			
			if($response !== "")
			{
				break;
			}
		}

		return $response;
	}
	
	public function findCategoryFromProduct(int $prodId)
	{
		$response = NULL;
		
		if(array_key_exists($prodId, $this->_productCategory))
		{
			$response = $this->_productCategory[$prodId];
		}
		
		return $response;
	}
	
	private function _createClientRequest(string $url, string $format = Client::FORMAT_JSON)
	{
		$request = $this->_client->createRequest();
		$request
			->setUrl($url)
			->setFormat($format)
		;
		
		return $request;
	}
}
