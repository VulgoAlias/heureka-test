<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\API;

class SiteController extends Controller
{
	private $_api;
	
	public function __construct($id, $module)
	{
		parent::__construct($id, $module);
		$this->_api = new API();
	}
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		$categories = $this->_api->getCategoryList();
		
		foreach($categories as &$category)
		{
			$category['img_url'] = $this->_api->getCategoryPreviewImage($category['categoryId']);
		}
		
        return $this->render('index', ['categories' => $categories]);
    }

	public function actionCategory()
	{

		$catId = Yii::$app->request->get('id');
		$category = $this->_api->getCategoryDetail($catId);
		$categories = $this->_api->getCategoryList();
		$products = $this->_api->getCategoryProducts($catId, 0, 5);
		
		foreach($products as &$product)
		{
			$this->_api->completeProductFromOffers($product);
		}		
		
        return $this->render('category', 
			[
				'category' => $category,
				'products' => $products,
				'categories' => $categories
			]);
	}
	
	public function actionProduct()
	{
		$productId = Yii::$app->request->get('prod_id');
		$product = $this->_api->getProductDetail($productId);
		$category = $this->_api->getCategoryDetail($product['categoryId']);
		$this->_api->completeProductFromOffers($product);
		$offers = $this->_api->getProductOffers($productId);
		
		return $this->render('product', 
			[
				'product' => $product,
				'offers' => $offers,
				'category' => $category
			]
		);
	}
	
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    
}
