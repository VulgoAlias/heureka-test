<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Homepage';
?>
<div class="site-index">
	<div id="side-panel">
			<ul>
			<?php foreach($categories as $category) { ?>
				<li class="side-panel-item ">
					<a href="/category/<?= $category['categoryId'] ?>" class="btn btn-primary">
						<?= $category['title'] ?>
					</a>		
				</li>
			<?php } ?>
			</ul>			
	</div>
	<div id="content">
		<div id="tile-holder" class="tiles">
			<ul>
			<?php foreach($categories as $category) { ?>
				<li class="category-tile">
					<a href="/category/<?= $category['categoryId'] ?>" class="">
						<img src="<?= $category['img_url'] ?>" alt="Náhledový obrázek kategorie <?= $category['title'] ?>">
						<strong><?= $category['title'] ?></strong>
					</a>		
				</li>
			<?php } ?>
			</ul>
		</div>
		<div class="pagination">
			
		</div>
	</div>
</div>
