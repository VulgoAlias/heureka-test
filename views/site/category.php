<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Kategorie';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="site-category">
	<div id="side-panel">
			<ul>
			<?php foreach($categories as $category) { ?>
				<li class="side-panel-item ">
					<a href="/category/<?= $category['categoryId'] ?>" class="btn btn-primary">
						<?= $category['title'] ?>
					</a>		
				</li>
			<?php } ?>
			</ul>			
	</div>
	<div class="content">
		<ul class="product-list">
			<?php foreach($products as $product) { ?>
			<li>
				<div class="product-card">
					<div class="product-card-left">
						<div class="product-card-image-holder">
							<img alt="Foto produktu <?= $product['title'] ?>" src="<?= $product['img_url'] ?>">
						</div>
					</div>
					<div class="product-card-center">
						<h3 clas="product-card-title"><?= $product['title']?></h3>
						<p class="product-card-description">
							<?= $product['description'] ?>
						</p>
					</div>
					<div class="product-card-right">
						<div class="product-card-price-holder">
							<span class="product-price-low">250</span>
							<span> - </span>
							<span class="product-price-high">350</span> 
							<span class="product-price-currency">Kč</span>
						</div>
						<div class="product-card-actions">
							<a href="/product/<?= $product['productId'] ?>" class="compare-price-btn btn btn-primary">Porovnat ceny</a>
						</div>
					</div>
				</div>
			</li>
			<?php } ?>
		</ul>
	</div>
</div>
