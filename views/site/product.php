<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Detail produktu '. $product['title'];
$this->params['breadcrumbs'][] = $category['title'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-product">

	<div class="top">
		<div class="pd-image-holder">
			<div class="pd-preview-image-holder">
				<img class="pd-preview-image" alt="Foto produktu <?= $product['title']?>" src="<?= $product['img_url'] ?>">
			</div>
			<div class="pd-image-previews">
			<?php foreach($offers as $offer) { 
				if(!$offer['img_url'] || $offer['img_url'] === $product['img_url'])
				{
					continue;
				} ?>
				<img class="pd-preview-image" alt="Foto produktu <?= $product['title']?>" src="<?= $offer['img_url'] ?>">
			<?php } ?>
			</div>
		</div>
		<div class="pd-text">
			<h2><?= $product['title'] ?></h2>
			<p><?= $product['description'] ?></p>
		</div>
	</div>
	<div class="bottom">
		<table class="offers">
			<?php foreach($offers as $offer) { ?>
				<tr class="offer-item" oid="<?= $offer['offerId'] ?>">
					<td class="oi-name"><?= $offer['title']?></td>
					<td class="oi-action">
						<a class="oi-buy-btn btn btn-warning" href="<?= $offer['url'] ?>">Koupit</a>
					</td>
					<td class="oi-price"><?= $offer['price'] ?></td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>
